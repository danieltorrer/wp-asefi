<?php get_header(); ?>
	<div class="container-fluid margin-main bg-azul contenedor" >
			
		<div class="row">
			<div class="col-md-10 col-md-offset-1 caja-container">
					<!-- <div class="col-md-4 col-sm-6"> -->

<!-- 					<div class="caja-servicio caja-consultoria caja-1">-->
					<div class="caja-servicio caja-consultoria">
						<figure class="effect-sarah">
							<img src="<?php echo get_template_directory_uri(); ?>/img/pueblae_m.jpg" alt="img01"/>
							<figcaption>
								<h3>Mercadotecnia y Dirección creativa</h3>
								<article>
									<ul>
										<li>Diseño de la representación visual</li>
										<li>Restyling de marca</li>
										<li>Manuales de identidad</li>
										<li>Desarrollo de todos los elementos de comunicación de una empresa, papelería corporativa, tipografía, colores, etc.</li>
									</ul>
								</article>
							</figcaption>
						</figure>
					</div>
					<!-- </div> -->
					<!-- <div class="col-md-4 col-sm-6"> -->
						<div class="caja-servicio caja-consultoria">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/escala1.jpg" alt="img01"/>
								<figcaption>
									<h3>Dirección de Arte</h3>
									<article>
										<ul>
											<li>Identidad visual general</li>
											<li>Guía creativa en la creación de contenido multimedia</li>
											<li>Desarrollo multimedia</li>
											<li>Publicaciones en redes sociales, comerciales y editoriales.</li>
										</ul>
									</article>
									<!--<a href="#">View more</a>-->
								</figcaption>
							</figure>
						</div>
					<!-- </div> -->
					<!-- <div class="col-md-4 col-sm-6"> -->
						<div class="caja-servicio caja-consultoria">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/05t_m.jpg" alt="img01"/>
								<figcaption>
									<h3>Multimedia y edición</h3>
									<article>
										<ul>
											<li>Fotografía digital</li>
											<li>Video de alta calidad</li>
											<li>Edición profesional de material audiovisual</li>
										</ul>
									</article>
								</figcaption>
							</figure>
						</div>
					<!-- </div> -->
					<!-- <div class="col-md-4 col-sm-6"> -->
						<div class="caja-servicio caja-consultoria">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/escala3.jpg" alt="img01"/>		
								<figcaption>
									<h3>Estrategia en redes sociales</h3>
									<article>
										<ul>
											<li>Contenido de valor vinculado con el usuario y el mercado actual</li>
											<li>Evaluación del contexto de la marca mediante una investigación constante</li>
											<li>Desarrollo de estrategias digitales, involucrando tendencias, diseño personalizado y material visual creativo</li>
											<li>Gestionamos la administración e interacción de los contenidos</li>
											<!--<li>Generamos conexión emocional con nuestro público meta</li>-->
										</ul>
									</article>
								</figcaption>
							</figure>
						</div>
					<!-- </div> -->
					<!-- <div class="col-md-4 col-sm-6"> -->
						<div class="caja-servicio caja-consultoria">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/escala4.jpg" alt="img01"/>
								<figcaption>
									<h3>Desarrollo web</h3>
									<article>
										<ul>
											<li>Diseño personalizado y único en armonía con la marca</li>
											<li>No utilizamos plantillas</li>
											<li>Programación avanzada compatible con todos los dispositivos móviles de actualidad</li>
											<li>Utilizamos el lenguaje más novedoso HTML5 que nos permite una experiencia fluida.</li>
											<li>Generamos páginas a tu medida. “Creamos paginas para cubrir necesidades”</li>
										</ul>
									</article>
								</figcaption>
							</figure>
						</div>
					<!-- </div> -->
					<!--<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-consultoria caja-6">
							<figure class="effect-sarah">
								<figcaption>
									<h3>Nombre del servicio</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit omnis minima alias eveniet aut ut dolore a quisquam veniam animi nisi vero, accusamus nemo.</p>
								</figcaption>
							</figure>
						</div>
					</div>-->
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
