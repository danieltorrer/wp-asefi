<!-- footer -->
<footer class="footer" role="contentinfo">

	<div class="container-fluid">
		<div class="row">
			<div class="container-footer col-sm-3 col-xs-7 col-md-2 col-md-offset-10 col-xs-offset-5 col-sm-offset-9">
				<?php if ( is_page( 'portafolio' )  || is_singular( 'portafolio' ) ) { ?>
					<div class="social-servicios">
						
						<a href="https://www.facebook.com/asefiAC/" target="_blank">
							<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-01.png" alt="">
							<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-05.png" alt="">
						</a>
						<a href="https://twitter.com/TuMercadologo?lang=es" target="_blank">
							<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-02.png" alt="">
							<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-04.png" alt="">

						</a>
					</div>
				<?php } ?>				

				<?php if ( is_page( array('contacto', 'historia', 'contaduria') ) || is_front_page() ) { ?>
					<div class="social-servicios mb-30">
						<a href="https://www.facebook.com/asefiContadores/" target="_blank">
							<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-01.png" alt="">
							<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-05.png" alt="">
						</a>
						<a href="https://twitter.com/SomosTuContador?lang=es" target="_blank">
							<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-02.png" alt="">
							<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-04.png" alt="">
						</a>
						<a href="https://mx.linkedin.com/company/asefi-contadores-y-asesores" target="_blank">
							<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-03.png" alt="">
							<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-06.png" alt="">
						</a>
					</div>
				<?php } ?>

<!--				<a target="_blank" href="https://www.facebook.com/Asefi-Contadores-y-Asesores-538097796331978/timeline/"><img src="<?php echo get_template_directory_uri(); ?>/img/icon_fb.png" alt=""></a>
				<a target="_blank" href="https://twitter.com/somostucontador"><img src="<?php echo get_template_directory_uri(); ?>/img/icon_tw_231.png" alt=""></a>
				<a target="_blank" href="https://www.linkedin.com/company/asefi-contadores-y-asesores"><img src="<?php echo get_template_directory_uri(); ?>/img/icon_ln.png" alt=""></a>-->
				<!-- <?php if ( !is_front_page( ) ) { ?>
					<p class="text-right aviso-de-privacidad">
						<a href="http://asefisc.com/aviso-de-privacidad/">Aviso de privacidad</a>
					</p>
				<?php } ?> -->
			</div>
			<?php if ( !is_front_page( ) ) { ?>
			<div class="col-sm-12">
					<p class="text-left aviso-de-privacidad">
						<a href="http://asefisc.com/aviso-de-privacidad/">Aviso de privacidad</a>
					</p>
			</div>
			<?php } ?>
		</div>
	</div>

</footer>
<!-- /footer -->

<?php wp_footer(); ?>

<!-- analytics -->
<!--<script>
(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
ga('send', 'pageview');
</script>-->

<!-- Latest compiled and minified JavaScript -->
<!--<script src="https://cdn.jsdelivr.net/g/conditionizr@4.5.1,modernizr@2.8.3"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>


<?php if( is_front_page()){ ?>
	<script>
	$(document).ready(function (){
		// var videoNumber = ( parseInt(Math.random() * 10) % 2 ) + 1;
		// $(".video-bg").attr('src', '<?php echo get_template_directory_uri(); ?>/video-asefi-' + videoNumber +  '.mp4');
		// $(".video- bg").load;
	})
	</script>
	<script>
	// 2. This code loads the IFrame Player API code asynchronously.
	var tag = document.createElement('script');

	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	// 3. This function creates an <iframe> (and YouTube player)
	//    after the API code downloads.
	var player;
	function onYouTubeIframeAPIReady() {
		player = new YT.Player('player', {
			height:'100%',
			width: '100%',
			fitToBackground: true,
			videoId: 'UfKnE_jKBE8',
			playerVars: {
				'autoplay': 1,
				'controls': 0,
				'autohide':1,
				'enablejsapi':1,
				'loop':1,
				'disablekb':1,
				'fs': 0,
				'modestbranding': 0,
				'showinfo': 0,
				'color': 'white',
				'theme': 'light',
				'rel':0  ,
				'playlist': 'UfKnE_jKBE8'
			},
			events: {
				'onReady': onPlayerReady
			}
		});
	}

	// 4. The API will call this function when the video player is ready.
	function onPlayerReady(event) {
		event.target.playVideo();
		player.mute();
		player.setVolume(0);
		//player.setSize(1920, 1080);
		player.setLoop(true);
		player.setPlaybackQuality('hd1080');
	}
	</script>


	<?php } ?>

	<?php if ( is_page('contacto') ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
		?>

		<script src="http://maps.google.com/maps/api/js?key=AIzaSyA7n9K3C55AM9kNMKf_yXMcnGNbp3lj0kA"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/gmaps.min.js"></script>
		<script>
		var map1, map2;
		var myLocation;
		var currentSelect = "Contacto";

		$(document).ready(function(){

			$('.asunto-input').change(function(e){
				if (e.target.value !== 'Bolsa de trabajo') {
					if (currentSelect == 'Bolsa de trabajo') {
						$('.cv-input').toggleClass('hidden');
					}
				} else {
					$('.cv-input').toggleClass('hidden');
				}
				currentSelect = e.target.value

			});

			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				e.target // newly activated tab

				if (e.target.className == 'fixMap') {
					$("#mapa-2").css("width", "100%");
					$("#mapa-2").css("height", "300px");
					map2.refresh();
					//google.maps.event.trigger(map2, 'resize');
				}

			});

			map1 = new GMaps({
				el: '#mapa-1',
				lat: 19.0285071,
				lng: -98.2059657
			});

			map2 = new GMaps({
				el: '#mapa-2',
				//lat: 18.993445,
				lat: 19.0027996,
				//lng: -98.2787168
				lng: -98.2934392
			});

			map2.addMarker({
				lat: 18.993445,
				lng: -98.2787168,
				title: 'ASEFI Sonata',
				infoWindow: {
					content: '<p class="infoWindow-text">ASEFI Sonata</p>'
				}
			});


			map1.addMarker({
				lat: 19.0285071,
				lng: -98.2059657,
				title: 'ASEFI Huexotitla',
				infoWindow: {
					content: '<p class="infoWindow-text">ASEFI Huexotitla</p>'
				}
			});

			map2.refresh();

			GMaps.geolocate({
				success: function(position) {
			    	myLocation =  position;
			    	console.log(myLocation);
				},
				error: function(error) {
					//$('.gps-msj').html('Por favor habilita el GPS');
			    	//alert('Geolocation failed: '+error.message);
				},
				not_supported: function() {
			    	$('.gps-msj').html('Tu navegador no soporta ');
				},
				always: function() {
			 		//alert("Done!");
				}
			});

			$('.route-sucursal').click(function(){
				$('.instructions').html('');
				var sucursal = $(this).attr('data-sucursal');
				console.log(sucursal);

				var destino = sucursal == 1 ? [19.0285071, -98.2059657] : [ 18.993445, -98.2787168];
				var mapaViaje = sucursal == 1? map1 : map2;

				mapaViaje.travelRoute({
					origin: [myLocation.coords.latitude, myLocation.coords.longitude],
					destination: destino,
					travelMode: 'driving',
					step: function(e) {
    					$('.instructions').append('<li>'+e.instructions+'</li>');
    					$('.instructions li:eq(' + e.step_number + ')').delay(1000 * e.step_number).fadeIn(400, function() {
    							mapaViaje.setCenter(e.end_location.lat(), e.end_location.lng());
      							mapaViaje.drawPolyline({
        							path: e.path,
									strokeColor: '#131540',
						    		strokeOpacity: 0.6,
						    		strokeWeight: 6
							});
						});
					}
				})
			});
		})

		</script>
		<?php } ?>


		<script>
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $('header').outerHeight()

		$(window).scroll(function(event){
			didScroll = true;
		});

		new WOW().init();

		setInterval(function() {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var st = $(this).scrollTop();

			// Make sure they scroll more than delta
			if(Math.abs(lastScrollTop - st) <= delta)
			return;

			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (st > lastScrollTop && st > navbarHeight){
				// Scroll Down
				$('header').removeClass('nav-down').addClass('nav-up');
			} else {
				// Scroll Up
				if(st + $(window).height() < $(document).height()) {
					$('header').removeClass('nav-up').addClass('nav-down');
				}
			}

			lastScrollTop = st;
		}
		</script>

		<?php if ( is_page( array('contaduria', 'consultoria' ) ) ) { ?>
			<script>
				$(document).ready(function(){
					$('.effect-sarah').addClass("sim-hover");

					setTimeout(function() {
						$('.effect-sarah').removeClass("sim-hover");						
					}, 500);
				})
			</script>
		<?php } ?>

	</body>
	</html>
