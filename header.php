<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		
		<?php wp_head(); ?>

		<?php
    	if (is_page('nosotros')) {
				if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        		wpcf7_enqueue_styles();
    		}    		
    	}
		?>

		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen, projection" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/animate.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css" />
		<!--<script>
		// conditionizr.com
		// configure environment tests
		conditionizr.config({
			assets: '<?php echo get_template_directory_uri(); ?>',
			tests: {}
		});
		</script>-->

	</head>
	<body <?php body_class(); ?>>


			<!-- header -->
			<header role="banner" class="nav-down">
				<nav id="navbar-primary" class="navbar" role="navigation">
				  <div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<a href="<?php echo get_bloginfo('url') ?>">
							<img class="pull-left logo-responsive hidden-md hidden-lg hidden-sm" src="<?php echo get_template_directory_uri(); ?>/img/asefi_logo_header_231.png">
						</a>
					  	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="collapse navbar-collapse" id="navbar-primary-collapse">
					  
						<div class="hidden-md hidden-lg hidden-sm mobile-menu">
							<!--<div class="logo">
								<a href="#">
									<img id="logo-navbar-middle" src="<?php echo get_template_directory_uri(); ?>/img/asefi_logo_header.png" alt="logo">
								</a>
							</div>-->
							<ul>
								<?php 
								wp_nav_menu(
								array(
									'theme_location'  => 'header-menu',
									'menu'            => '',
									'container'       => '',
									'container_class' => 'menu-{menu slug}-container',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => ''
									)
								);
								 ?>
	
								<?php 
								wp_nav_menu(
								array(
									'theme_location'  => 'sidebar-menu',
									'menu'            => '',
									'container'       => 'div',
									'container_class' => 'menu-{menu slug}-container',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => ''
									)
								);
								 ?>
							</ul>
						</div>

						<ul class="nav navbar-nav hidden-xs">
							<span class="list list-left">
								<?php 
								wp_nav_menu(
								array(
									'theme_location'  => 'header-menu',
									'menu'            => '',
									'container'       => 'li',
									'container_class' => 'menu-{menu slug}-container',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => ''
									)
								);
								 ?>
								<!--<li><a href="#">Link</a></li>
								<li><a href="#">Link</a></li>-->
								<span class="triangle-right"></span>
							</span>
							<li class="logo logo-2 wow fadeIn">
								<a href="<?php echo get_bloginfo('url') ?>">
									<img id="logo-navbar-middle" src="<?php echo get_template_directory_uri(); ?>/img/asefi_logo_header_231.png" alt="logo">
								</a>
							</li>
							<span class="list list-right">
								<span class="triangle-left"></span>
								<?php 
								wp_nav_menu(
								array(
									'theme_location'  => 'sidebar-menu',
									'menu'            => '',
									'container'       => 'li',
									'container_class' => 'menu-{menu slug}-container',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => ''
									)
								);
								 ?>
								
							</span>
						</ul>
					</div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				</header><!-- header role="banner" -->
			
			<!-- /header -->
