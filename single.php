<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="container margin-main">
    <div class="row">
        <div class="col-md-4">
            <h4>Tags</h4>
            <?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
            <br>
            <h4>Fecha</h4>
            <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
            <p><?php edit_post_link(); // Always handy to have Edit Post Links available ?></p>
			      
        </div>
        <div class="col-md-8">
            <div class="client-description">
							<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
            	<div class="post-image" style="background: url('<?php echo $url?>') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
            		<div class="post-title">
            			<h1><?php the_title(); ?></h1>
            		</div>
            	</div>
            	<br><br><br>
              <p><?php the_content(); ?></p>
              
							<br>
							<hr>
				      <p><?php _e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>
				
				      <p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>
            </div>
        </div>
    </div>
</div>

<?php endwhile; else: 
  ?>
  <?php endif; 
  ?>

<?php get_footer(); ?>
