<?php get_header(); ?>

	<main role="main" class="container margin-main">

		<div class="row historia-bottom">
			<div class="col-md-5 col-sm-12">
				<div class="historia-container">
					<div class="title-ribbon contaduria wow fadeInUp">
						<h4>HISTORIA</h4>
					</div>
					<div class="historia-descripcion wow fadeInUp">
						<p>asefi Contadores y Asesores S.C es la firma de servicios que ofrece soluciones con la finalidad de impulsar el crecimiento en el ámbito de los negocios, somos especialistas en el área: contable, derecho fiscal, administración, diseño y comunicación empresarial.</p>
						<p>Con experiencia en el acompañamiento y consultoría para alcanzar objetivos, generamos
						proyectos de alto impacto y retorno, brindando resultados medibles. Desarrollamos,
						conjuntamente con nuestros clientes, un servicio personalizado para implementar una gran
						cantidad de horas consultor para ellos, demostramos en la práctica nuestro compromiso por
						generar valor, así como gran consistencia en nuestros servicios.</p>

						<p>Contamos con más de 30 colaboradores preparados, que generan  valor en todos nuestros proyectos en más de 18 años, asefi Contadores y Asesores S.C acumula una historia de éxito con más de 70 clientes en México.</p>
					</div>
				</div>
			</div>

			<div class="col-md-7 col-sm-12">
				<div class="historia-imagen text-right wow fadeInUp">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/asefi-segundo.jpg" alt="Historia">
				</div>
			</div>

		</div>

		<div class="row">


			<div class="col-md-7 col-sm-12">
				<div class="historia-imagen text-left wow fadeInUp">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/asefi-contador.jpg" alt="Historia">
				</div>
			</div>
			
			<div class="col-md-5 col-sm-12">
				<div class="historia-container">
					<div class="title-ribbon consultoria wow fadeInUp">
						<h4>SOMOS</h4>
					</div>
					<div class="historia-descripcion wow fadeInUp">
					<p>Una firma de servicios empresariales distinguido por contar con un equipo de profesionistas con alto grado de responsabilidad.</p>
					</div>
				</div>
				<div class="historia-container">
					<div class="title-ribbon contaduria  wow fadeInUp">
						<h4>TRABAJAMOS</h4>
					</div>
					<div class="historia-descripcion wow fadeInUp">
					<p>Productivamente, con entusiasmo, organización, eficiencia y perseverancia.</p>
					</div>
				</div>
				<div class="historia-container">
					<div class="title-ribbon consultoria wow fadeInUp">
						<h4>LOGRAMOS</h4>
					</div>
					<div class="historia-descripcion wow fadeInUp">
					<p>Que nuestros clientes depositen su confianza en nosotros, participamos con ellos ellos en proyectos rentables para contribuir con Méxicoy su gente.</p>
					</div>
				</div>
			</div>

		</div>

	</main>


<?php get_footer(); ?>
