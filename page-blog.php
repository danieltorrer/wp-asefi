<?php get_header(); ?>

<div class="container margin-main">
    <div class="row">
      <?php 
        $args = array(
          'post_type' => 'post',
        );
        $the_query = new WP_Query( $args);
      ?>
      <?php if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
      ?>
      
      <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
      <div id="id<?php the_id();?>" class="col-md-6" >
        <div class="client-container" style="background: url('<?php echo $url?>') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
          <a href="<?php the_permalink();?>" class="full-link"></a>
          <div class="client-description contaduria">
            <div class="client-description-content">
              <a href="<?php the_permalink();?>">
                <h3>
                  <?php the_title()?>
                </h3>  
              </a>
              <p>Branding, Social Media, Identidad Corporativa</p>  
            </div>
          </div>
        </div>
        
      </div>
      
      <?php endwhile; else: 
      ?>
      <?php endif; 
      ?>
      
    </div>
</div>

<?php get_footer(); ?>
