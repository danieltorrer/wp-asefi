<?php get_header(); ?>
	<div class="container margin-main bg-azul contenedor">

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-1">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/asefi-historia.jpg" alt="img01"/>
								<figcaption>
									<h3>Contabilidad y asesoría de negocios</h3>
									<article>
										<ul>
											<li>Actualización de registros contables y pagos de impuestos, obligaciones fiscales.</li>
											<li>Registros contables oportunos para emisión de informes financieros de acuerdo a las normas de información financieras</li>
											<li>Plan contable para empresas</li>
											<li>Asesorías y estrategias para un control financiero saludable</li>
										</ul>
									</article>
								</figcaption>
							</figure>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-2">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/sq_3_m.jpg" alt="img01"/>
								<figcaption>
									<h3>Nóminas y Seguridad Social</h3>
									<article>
										<ul>
											<li>Cálculo y elaboración de nóminas</li>
											<li>Control de movimientos ante IMSS, INFONAVIT, Finanzas</li>
											<li>Asesoría legal en Material Laboral</li>
											<li>Asistencia Personal y Profesional en diligencias</li>
											<li>Experiencia en manejo de SUA e IDSE</li>
										</ul>
									</article>
									<!--<a href="#">View more</a>-->
								</figcaption>
							</figure>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-3">
							<figure class="effect-sarah">
								<img src="<?php echo get_template_directory_uri(); ?>/img/sq_2_m.jpg" alt="img01" style="top: 0; left: -150px; position: absolute;"/>
								<figcaption>
									<h3>Asesoría Fiscal</h3>
									<article>
										<ul>
											<li>Situación en el RFC</li>
											<li>Diagnóstico para elegir el Régimen correcto de Tributación</li>
											<li>Cumplimiento Óptimo de Obligaciones Fiscales</li>
											<li>Pagos de impuestos justos con respaldo que cumpla los requisitos fiscales</li>
											<li>Asesoría Jurídica</li>
										</ul>
									</article>
									<!--<a href="#">View more</a>-->
								</figcaption>
							</figure>
						</div>
					</div>
					<!--
					<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-4">
							<figure class="effect-sarah">
								<figcaption>
									<h3>Nombre del servicio</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit omnis minima alias eveniet aut ut dolore a quisquam veniam animi nisi vero, accusamus nemo.</p>
								</figcaption>
							</figure>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-5">
							<figure class="effect-sarah">
								<figcaption>
									<h3>Nombre del servicio</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit omnis minima alias eveniet aut ut dolore a quisquam veniam animi nisi vero, accusamus nemo.</p>
								</figcaption>
							</figure>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="caja-servicio caja-6">
							<figure class="effect-sarah">
								<figcaption>
									<h3>Nombre del servicio</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit omnis minima alias eveniet aut ut dolore a quisquam veniam animi nisi vero, accusamus nemo.</p>
								</figcaption>
							</figure>
						</div>
					</div>
					-->
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
