<?php get_header(); ?>

	<main role="main" class="container margin-main">

		<div class="row historia-bottom">
			<div class="col-sm-12">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(  ) ?>
					<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
			</div>

		</div>

	</main>


<?php get_footer(); ?>
