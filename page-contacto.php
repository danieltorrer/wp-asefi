<?php get_header(); ?>
	<div class="container mt-60">
		<div class="row">
			<div class="col-md-6">
				<h1 class="wow fadeIn">Contáctanos</h1>
				<h5 class="wow fadeIn">Estaremos encantados de ayudarte</h5>
				<div class="form-horizontal">
					
					<!--<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(  ) ?>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>	-->

					<form method="POST" action="http://formspree.io/contacto@asefisc.com" class="wow fadeIn">

					<div class="form-group">
					    <label for="" class="col-sm-2 control-label">Nombre*:</label><div class="col-sm-10"><input name="text-658" type="text" class="form-control" placeholder="Nombre"></div>
					</div>
					<div class="form-group">
					    <label for="" class="col-sm-2 control-label">E-mail*:</label><div class="col-sm-10"><input type="email" name="email-225" class="form-control" placeholder="E-mail"></div>
					</div>                  
					<div class="form-group">
					    <label for="" class="col-sm-2 control-label">Asunto*:</label><div class="col-sm-10"><select name="menu-631" class="form-control asunto-input">
					    	<option value="Contacto">Contacto</option>
					    	<option value="Contabilidad">Contabilidad</option>
					    	<option value="Fiscal">Fiscal</option>
					    	<option value="Diseño empresarial">Diseño empresarial</option>
					    	<option value="Redes sociales">Redes sociales</option>
					    	<option value="Bolsa de trabajo">Bolsa de trabajo</option>
					    </select></div>
					</div>

					<!--<div class="form-group hidden cv-input">
					  <label for="" class="col-sm-2 control-label">C.V.</label><div class="col-sm-10"><input type="text">[file file-93 class:form-control filetypes:.docx|.pdf|.doc]</div>
					</div>-->

					<div class="form-group">
					    <label for="" class="col-sm-2 control-label">Comentario:</label><div class="col-sm-10"><textarea name="textarea-304" cols="30" rows="10"  placeholder="Comentarios, sugerencias" class="form-control"></textarea></div></div>

					<div class="form-group">
					  <div class="col-sm-2"></div>
					  <div class="col-sm-10">
					  	<button type="submit" class="btn btn-success pull-right btn-lg">
					  		Enviar
					  	</button>
					  </div>
					</div>


					</form>
				
				</div>
			</div>

			<div class="col-md-6 wow fadeIn">
				<ul id="myTabs" class="nav nav-tabs">
					<li class="active">
						<a data-toggle="tab" href="#sucursal-1">
							<address>
								<strong>Huexotitla</strong>
							</address>
						</a>
					</li>
					
					<li>
						<a data-toggle="tab" href="#sucursal-2" class="fixMap">
							<address>
								<strong>Sonata</strong><br>
							</address>
						</a>
					</li>
				</ul>

				<div class="tab-content">	
					<div class="tab-pane active" id="sucursal-1">
						<div class="map" id="mapa-1"></div>
						<p>39 Oriente. Num 19. Int 301<br>
							Col. Huexotitla <br>
								<a href="tel:2948933">Tel. 2.94.89.33</a> <br>
								Puebla, Pue
						</p>
						<a href="#" class="btn btn-primary btn-success route-sucursal" data-sucursal="1" >Trazar ruta</a>
						<p class="gps-msj"></p>
						<ul class="instructions"></ul>

					</div>
					
					<div class="tab-pane" id="sucursal-2">
						<div class="map" id="mapa-2"></div>
						<p>
							Edificio Escala Primer piso. <br> 
							Paseo Ópera 4, Lomas de Angelópolis <br>
							<a href="tel:6441612">Tel. 6.44.16.12</a> <br>
							72830 San Andrés Cholula, Puebla.
						</p>

						<a href="#" class="btn btn-primary btn-success route-sucursal" data-sucursal="2">Trazar ruta</a>
						<p class="gps-msj"></p>
						<ul class="instructions"></ul>
					</div>

				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>