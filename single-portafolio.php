<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="container margin-main">
    <div class="row">
        <div class="col-md-4">
            <h4>Cliente</h4>
            <img src="<?php echo get("logo"); ?>" alt="">
            <br>
            <br>
            <h4>Servicios</h4>
            <h3>
              <?php for ($i=0; $i < get_count_field("servicio"); $i++) {
                  $index = $i + 1;
                  $field = get_field("servicio");
                  echo $field[$index];
                  echo "<br>";
              } ?>
            </h3>
            <br>
            <br>
            
            <?php if ( get("fecha_inicio") ) { ?>
              <h4>Fecha</h4>
              <h2> <?php echo get("fecha_inicio"); ?>
              <?php if ( get("fecha_fin") ){?>
                // 
              <?php } ?>
              <?php echo get("fecha_fin"); ?> 

            <?php if( get("active") ){ ?>
              <img src="<?php echo get_template_directory_uri(); ?>/img/activo.png" alt="" style="margin-left: 15px; margin-top: -10px;">
            <?php } ?>

              </h2> 
            <?php } ?>

            <p><?php edit_post_link(); // Always handy to have Edit Post Links available ?></p>
        </div>
        <div class="col-md-8">
            <div class="client-description">
              <h1><?php the_title(); ?></h1>
              <p><?php the_content(); ?></p>
              <!--<div id="mycarousel" class="carousel slide" data-ride="carousel" data-interval="false">-->
                    <!-- Indicators -->
<!--                     <ol class="carousel-indicators">
                      <?php
                        $cont = 0;
                        $slides = get_group('slide');
                        foreach($slides as $slide){
                          ?>
                          <li data-target="#mycarousel" data-slide-to="<?php echo $cont?>" 
                              <?php if ($cont == 0){ ?>
                                class="active"
                              <?php } ?>        
                          </li>
                        <?php 
                        $cont++;
                        }
                      ?>
                    </ol> -->

                    <!-- Wrapper for slides -->
                    <!-- <div class="carousel-inner" role="listbox">

                      <?php
                        $cont = 0;
                        $slides = get_group('slide');
                        foreach($slides as $slide){
                          ?>

                            <?php if ($cont == 0){ ?>
                              <div class="item active">
                            <?php }else{?>
                              <div class="item">
                            <?php }?>     

                            <?php 
                              if ($slide['slide_tipo'][1] == "Imagen") {
                                ?>    
                                	<img src="<?php echo $slide['slide_imagen'][1]['original'] ?>" alt="">  
                                  <div style="height: 550px; 
                                              background: url() no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                                  </div>
                                <?php
                              }
                              else{
                                echo $slide['slide_video'][1];
                              }
                            ?> 
                            <div class="carousel-caption">
                              <h3> <?php echo $slide['slide_descripcion'][1] ?></h3>
                            </div>

                          </div>

                        <?php 
                        $cont++;
                        }
                      ?>
                      
                    </div> -->

                    <!-- Controls -->
<!--                     <a class="left carousel-control" href="#mycarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </a>
                    <a class="right carousel-control" href="#mycarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    </a> -->
              <!--</div>-->

              <?php 
                  $cont = 0;
                  $slides = get_group('slide');
                  foreach($slides as $slide){
                      if ($slide['slide_tipo'][1] == "Imagen") {?>    
                        <a data-fancybox="gallery" href="<?php echo $slide['slide_imagen'][1]['original'] ?>" data-caption="<?php echo $slide['slide_descripcion'][1] ?>" >
                          <img src="<?php echo $slide['slide_imagen'][1]['thumb'] ?>" class="fancy-image">
                        </a>
                      <?php 
                      } else { ?>

                        <a data-fancybox="gallery" href="<?php echo $slide['slide_video'][1]; ?>" data-caption="<?php echo $slide['slide_descripcion'][1] ?>" >
                          Video
                        </a>
                      <?php
                      }
                  }
               ?>
            </div>
        </div>
    </div>
</div>

<?php endwhile; else: 
  ?>
  <?php endif; 
  ?>

<?php get_footer(); ?>
