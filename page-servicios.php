<?php get_header(); ?>

<main role="main" class="container-fluid">

	<div class="container margin-main">
		<div class="row">
			<div class="col-md-5 col-md-offset-1 col-xs-12 ">
				<div class="color-fondo1 servicios-square wow fadeInUp">
					<p class="text-center">
						<a href="<?php echo site_url(); ?>/contaduria/"><img src="<?php echo get_template_directory_uri()?>/img/iconos-contaduria.png" alt="" class="icono-servicios"></a>
					</p>
					<div>
						<a href="<?php echo site_url(); ?>/contaduria/"><h1 class="text-center">CONTABILIDAD <br>Y ASESORÍA DE <br> NEGOCIOS</h1></a>
					</div>
				</div>
				<div class="social-servicios mb-30">
					<a href="https://www.facebook.com/asefiContadores/" target="_blank">
						<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-01.png" alt="">
						<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-05.png" alt="">
					</a>
					<a href="https://twitter.com/SomosTuContador?lang=es" target="_blank">
						<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-02.png" alt="">
						<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-04.png" alt="">
					</a>
					<a href="https://mx.linkedin.com/company/asefi-contadores-y-asesores" target="_blank">
						<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-03.png" alt="">
						<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-06.png" alt="">
					</a>
				</div>
			</div>
			<div class="col-md-5 col-xs-12 "><a href="#">
				<div class="color-fondo2 servicios-square wow fadeInUp">
					<p class="text-center">
						<a href="<?php echo site_url(); ?>/consultoria/"><img src="<?php echo get_template_directory_uri()?>/img/iconos-mercadotecnia.png" alt="" class="icono-servicios"></a>
					</p>
					<div>
						<a href="<?php echo site_url(); ?>/consultoria/"><h1 class="">MERCADOTECNIA <br>Y DIRECCIÓN <br>CREATIVA</h1></a>
					</div>
				</div>
				<div class="social-servicios text-right mb-30">
					<a href="https://www.facebook.com/asefiAC/" target="_blank">
						<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-01.png" alt="">
						<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-05.png" alt="">
					</a>
					<a href="https://twitter.com/TuMercadologo?lang=es" target="_blank">
						<img class="img-default" src="<?php echo get_template_directory_uri()?>/img/iconos-02.png" alt="">
						<img class="img-hover" src="<?php echo get_template_directory_uri()?>/img/iconos-04.png" alt="">

					</a>
				</div>  
			</div>
		</div>
	</div>
</main>

<div class="col-sm-12">
	<?php if ( !is_front_page( ) ) { ?>
		<p class="text-left aviso-de-privacidad">
			<a href="http://asefisc.com/aviso-de-privacidad/">Aviso de privacidad</a>
		</p>
	<?php } ?>
</div>

<?php get_footer(); ?>
